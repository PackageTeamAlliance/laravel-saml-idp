<?php

namespace Pta\LaravelSAML\Providers;

use Illuminate\Support\ServiceProvider;

class LaravelSAMLServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerViews();
        $this->registerConfig();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerViews();
        $this->registerConfig();
    }

    protected function registerConfig()
    {
        $this->publishes([realpath(__DIR__ . '/../../config/config.php') => config_path('saml.php'), 'config']);

        $mergePath = realpath(__DIR__ . '/../../config/config.php');

        $this->mergeConfigFrom($mergePath, 'saml');
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $this->loadViewsFrom(realpath(__DIR__ . '/../../resources/views'), 'pta/saml');

        $this->publishes([realpath(__DIR__ . '/../../resources/views') => base_path('resources/views/vendor/saml'), ]);
    }
    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }
}
