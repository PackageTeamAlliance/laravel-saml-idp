<?php namespace Pta\LaravelSAML\Services\Auth\Traits;

use App\Services\Access\Traits\RegistersUsers;

/**
 * Class AuthenticatesAndRegistersUsers
 * @package App\Services\Access\Traits
 */
trait AuthenticatesAndRegistersUsers
{
    use AuthenticatesUsers, RegistersUsers {
        AuthenticatesUsers::redirectPath insteadof RegistersUsers;
    }
}
