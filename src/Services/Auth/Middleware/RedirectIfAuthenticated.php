<?php

namespace Pta\LaravelSAML\Services\Auth\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Pta\LaravelSAML\Services\SamlAuth;

/**
 * Class RedirectIfAuthenticated
 * @package App\Http\Middleware
 */
class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        if (Auth::guard($guard)->check()) {
            if(isset($request['SAMLRequest'])){
                new SamlAuth($request);
            }else {
                return redirect('/');
            }
        }

        return $next($request);
    }
}
