# Laravel SAML IDP Authentication Driver

[![Latest Version on Packagist](https://img.shields.io/packagist/v/pta/laravel-saml-idp.svg?style=flat-square)](https://packagist.org/packages/pta/laravel-saml-idp)
[![Software License](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](LICENSE.md)
[![Build Status](https://img.shields.io/travis/pta/laravel-saml-idp/master.svg?style=flat-square)](https://travis-ci.org/pta/laravel-saml-idp)
[![Coverage Status](https://img.shields.io/scrutinizer/coverage/g/pta/laravel-saml-idp.svg?style=flat-square)](https://scrutinizer-ci.com/g/pta/laravel-saml-idp/code-structure)
[![Quality Score](https://img.shields.io/scrutinizer/g/pta/laravel-saml-idp.svg?style=flat-square)](https://scrutinizer-ci.com/g/pta/laravel-saml-idp)
[![Total Downloads](https://img.shields.io/packagist/dt/pta/laravel-saml-idp.svg?style=flat-square)](https://packagist.org/packages/pta/laravel-saml-idp)

This package adds a custom Authorization Driver to Laravel 5.2+ to allow your login system to behave as a SAML IDP. Specifically creating this for SSO Between Laravel Applications and Canvas LMS.

## Install

Via Composer

``` bash
$ composer require pta/laravel-saml-idp
```

## Usage

``` php
$laravel-saml-idp = new Pta\laravel-saml-idp();
echo $laravel-saml-idp->echoPhrase('Hello, League!');
```

## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Testing

``` bash
$ composer test
```

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Security

If you discover any security related issues, please email dr.parham@gmail.com instead of using the issue tracker.

## Credits

- [Dustin Parham](https://github.com/drparham)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
