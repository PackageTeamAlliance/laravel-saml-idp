<?php

return [
    'sp' => [
        base64_encode('https://lms.westcottcourses.com/login/saml') => [
            'destination' => 'https://lms.westcottcourses.com/saml_consume',
            'issuer' => 'http://idp.westcottcourses.com',
            'cert' => realpath(__DIR__ . '/../src/Certs/saml.crt'),
            'key' => realpath(__DIR__ . '/../src/Certs/saml.pem'),
        ]
    ]
];